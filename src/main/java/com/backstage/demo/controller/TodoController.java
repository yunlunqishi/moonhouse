package com.backstage.demo.controller;

import com.backstage.demo.model.News;
import com.backstage.demo.model.Version;
import com.backstage.demo.service.NewsService;
import com.backstage.demo.service.OpenMsgService;
import com.backstage.demo.service.OpenPicService;
import com.backstage.demo.service.VersionService;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
@RequestMapping("/todo")
public class TodoController {
    @Autowired
    private NewsService newsService;
    @Autowired
    private OpenMsgService openMsgService;
    @Autowired
    private VersionService versionService;

    @RequestMapping(value="/newsinfo")
    public String newsinfo( Integer newsId) {

        News news = newsService.selectByPrimaryKey(newsId);
        String dwe = news.getNewsContent();
        return dwe;
    }





//    @RequestMapping(value="/addShop")
//    public String  addShop(Model model, JshMaterial jshMaterial) {
//        jshMaterial.setRetailprice(Integer.valueOf(jshMaterial.getUnit()));
//        jshMaterial.setLowprice(Integer.valueOf(jshMaterial.getUnit()));
//        jshMaterial.setPresetpriceone(Integer.valueOf(jshMaterial.getUnit()));
//        jshMaterial.setPresetpricetwo(Integer.valueOf(jshMaterial.getUnit()));
//        jshMaterial.setTenantId(Long.valueOf(jshMaterial.getUnit()));
//
//        jshMaterial.setEnabled(true);
//        jshMaterial.setEnableserialnumber(String.valueOf(1));
//        jshMaterial.setModel(jshMaterial.getMfrs());
//               jshMaterialService.insertSelective(jshMaterial);
//        return "redirect:/shop-tables";
//    }
//    @RequestMapping(value="/addShoper")
//    public String  addShoper(Model model, Shop shop) {
//        shopService.insertSelective(shop);
//        return "redirect:/shoper-tables";
//    }
//    @RequestMapping(value="/addCode")
//    public String  addCode(Model model, ShopInfo shopInfo) {
//        ShopInfo shopInfo1 = shopInfoService.selectByshopIdAndMarialIdAndType(shopInfo.getShopId(),shopInfo.getShopMarialId(),shopInfo.getType());
//        if (shopInfo1==null) {
//            String code = "http://baidingthink.com/userInformation.html?money=" + shopInfo.getShopMarialId() + "&userid=" + shopInfo.getType() + "&shopId=" + shopInfo.getShopId();
//            shopInfo.setCode(code);
//            shopInfoService.insertSelective(shopInfo);
//        }
//        return "redirect:/shoper-tables";
//    }
//    @RequestMapping(value="/removeCode")
//    public String  removeCode(Model model, ShopInfo shopInfo) {
//      shopInfoService.deleteByPrimaryKey(shopInfo.getCodeId());
//        return "redirect:/shoper-tables";
//    }
//    /**
//     * 生成二维码方法
//     *
//     * @param  content 要生成的内容
//     * @param resp response对象
//     * @throws Exception 抛出异常
//     */
//    @RequestMapping(value = "/qrcode")
//    public void getQrcode(String content, HttpServletResponse resp) throws Exception {
//        ServletOutputStream stream = null;
//       /* Long qrid=Long.parseLong(content);
//        Ticket ticket=ticketRepository.findByTicketId(qrid);
//       if (ticket!=null) {*/
//        try {
//            stream = resp.getOutputStream();
//            Map<EncodeHintType, Object> hints = new HashMap<>();
//            //编码
//            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
//            //边框距
//            hints.put(EncodeHintType.MARGIN, 0);
//            QRCodeWriter qrCodeWriter = new QRCodeWriter();
//            BitMatrix bm = qrCodeWriter.encode(content, BarcodeFormat.QR_CODE, 200, 200, hints);
//            MatrixToImageWriter.writeToStream(bm, "png", stream);
//        } catch (WriterException e) {
//            e.getStackTrace();
//
//        } finally {
//            if (stream != null) {
//                stream.flush();
//                stream.close();
//            }
//        }
//    }
}
