package com.backstage.demo.controller.login;




import com.backstage.demo.model.User;
import com.backstage.demo.until.AesEncryptUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static com.backstage.demo.until.AesEncryptUtils.decrypt;

@RestController
public class LoginController {

    @RequestMapping("/login")
    public Object login(User user) throws Exception {
        String pa = user.getPassword();
        String pas =decrypt(pa, "hanyuehanyuehany");
        user.setPassword(pas);
        //添加用户认证信息
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(
                user.getUserName(),
                user.getPassword()
        );
        try {
            //进行验证，这里可以捕获异常，然后返回对应信息
            subject.login(usernamePasswordToken);
        } catch (AuthenticationException e) {
            e.printStackTrace();
            return "账号或密码错误！";
        } catch (AuthorizationException e) {
            e.printStackTrace();
            return "没有权限";
        }
        Map<String, Object> params= new HashMap<>();
        params.put("id","11");

        return params;
    }
    //注解验角色和权限
//    @RequiresRoles("admin")
//    @RequiresPermissions("add")
//    @RequestMapping("/index")
//    public String index() {
//        return "index";
//    }


}