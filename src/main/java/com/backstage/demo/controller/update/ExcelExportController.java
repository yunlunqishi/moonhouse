package com.backstage.demo.controller.update;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.backstage.demo.model.*;

import com.backstage.demo.until.ExcelUtil;
import com.backstage.demo.until.PoiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 * <pre>
 * &#64;author cao_wencao
 * &#64;date 2018年12月13日 下午6:16:59
 * </pre>
 */
@RestController
@RequestMapping("/excel/export")
public class ExcelExportController {



//    @GetMapping("/exportExcel")
//    public void export(HttpServletResponse response) throws IOException {
//        System.out.println(1);
//        // 模拟从数据库获取需要导出的数据
//        List<UserRun> list = userInfoService.selectAllRun();
//
//        // 导出操作
//        PoiUtils.exportExcel(list, "员工信息表", "员工信息", UserRun.class, "员工信息.xls", response);
//
//    }

//    @GetMapping("/export")
//    public void export1(HttpServletResponse response) throws IOException {
//        List<Member> list = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            list.add(new Member((long)i, "张三" + i, i % 2 + "", new Date(), 20 + i + "",
//                    "15219873928", "123456" + i));
//        }
//        //ExportParams可以通过构造的两个参数设置导出的sheet名字和excel的title列名
//        ExportParams params = new ExportParams();
//        Workbook workbook = ExcelExportUtil.exportExcel(params, Member.class, list);
//
//        // 告诉浏览器用什么软件可以打开此文件
//        response.setHeader("content-Type", "application/vnd.ms-excel");
//        // 下载文件的默认名称
//        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode("用户数据表","UTF-8") + ".xls");
//        //编码
//        response.setCharacterEncoding("UTF-8");
//        workbook.write(response.getOutputStream());
//    }
//@RequestMapping("export2")
//public void export2(HttpServletResponse response) throws Exception {
//
//    //模拟从数据库获取需要导出的数据
//    List<Person2> personList = new ArrayList<>();
//    Person2 person1 = new Person2("路飞","1",new Date());
//    Person2 person2 = new Person2("娜美","2", new Date());
//    Person2 person3 = new Person2("索隆","1", new Date());
//    Person2 person4 = new Person2("小狸猫","1", new Date());
//    personList.add(person1);
//    personList.add(person2);
//    personList.add(person3);
//    personList.add(person4);
//
//    //导出操作
//    ExcelUtil.exportExcel(personList,"花名册","草帽一伙",Person2.class,"海贼王.xls",response);
//}
//
//    @RequestMapping("/importExcel2")
//    public void importExcel2(@RequestParam("file") MultipartFile file) {
//        ImportParams importParams = new ImportParams();
//        // 数据处理
//        importParams.setHeadRows(1);
//        importParams.setTitleRows(1);
//
//        // 需要验证
//
//
//        try {
//            ExcelImportResult<JshSerialNumber> result = ExcelImportUtil.importExcelMore(file.getInputStream(), JshSerialNumber.class, importParams);
//
//            List<JshSerialNumber> successList = result.getList();
//            System.out.println("导入条数"+successList.size()+"条");
//            int i = 1;
//            for (JshSerialNumber demoExcel : successList) {
//            demoExcel.setIsSell(String.valueOf(0));
//            if ("36".equals(demoExcel.getRemark())){
//                demoExcel.setMaterialId(Long.valueOf(587));
//            }if ("712".equals(demoExcel.getRemark())){
//                    demoExcel.setMaterialId(Long.valueOf(588));
//                }
//            demoExcel.setCreateTime(new Date());
//                jshSerialNumberService.insertSelective(demoExcel);
//                System.out.println("条数"+i+"/"+successList.size());
//                i++;
//            }
//        } catch (IOException e) {
//        } catch (Exception e) {
//        }
//    }


}
