package com.backstage.demo.controller.view;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectRequest;
import com.backstage.demo.model.*;
import com.backstage.demo.model.vo.*;
import com.backstage.demo.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;


@Controller
@Slf4j
@RequestMapping("/")
public class ViewAction {



    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private RegionService regionService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private WxUserService wxUserService;
    @Autowired
    private OpenMsgService openMsgService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private OpenPicService openPicService;
    @Autowired
    private  VersionService versionService;



    @RequestMapping(value="/")
    public String index( Model model) {

        return "pages-sign-in";
    }

//    下面为接口路径
    @RequestMapping(value="/{name}")
    public String commonController(@PathVariable String name, Model model) {
        return name;
    }


    @RequestMapping(value="/dashboard")
    public String dashboard( Model model,HttpServletRequest request) {
        return "dashboard";
    }
    @RequestMapping(value="/news-tables")
    public String newstables( Model model,HttpServletRequest request) {
        List<News> list = newsService.selectList();
        model.addAttribute("list",list);
        return "/news-tables";
    }
    @RequestMapping(value="/wxuser-tables")
    public String wxusertables(Model model) {
        List<WxUser> list = wxUserService.selectList();
        model.addAttribute("list",list);
        return "wxuser-tables";
    }
    @RequestMapping(value="/user-tables")
    public String usertables(Model model) {
        List<User> list = userService.selectAll();
        model.addAttribute("list",list);
        return "user-tables";
    }

    @RequestMapping(value="/openmsg-tables")
    public String openmsgtables(Model model) {
        List<OpenMsgPage> list = openMsgService.selectList();
        model.addAttribute("list",list);
        return "openmsg-tables";
    }
    @RequestMapping(value="/comment-tables")
    public String commenttables(Model model) {
        List<CommentPage> list = commentService.selectList();
        model.addAttribute("list",list);
        return "comment-tables";
    }
    @RequestMapping(value="/version-tables")
    public String version(Model model) {
        List<Version> list = versionService.selectAll();
        model.addAttribute("list",list);
        return "version-tables";
    }



    @RequestMapping(value="/news-update")
    public String newstables( Model model,HttpServletRequest request,Integer newsId) {
        News news = newsService.selectByPrimaryKey(newsId);
        model.addAttribute("news",news);
        return "/news-update";
    }

    public String codeToString( String code) {

        String district = regionService.selectByCodeReturnString(code);

        return district;
    }
    @RequestMapping(value="/uploa")
    public Object upload( Model model,HttpServletRequest request,@RequestParam("fileName") MultipartFile[] files) {


        // 创建list集合，用于接收上传文件的路径
        List<String> filePathList = new ArrayList<String>();

        // 拼接文件上传位置，这里使用Tomcat服务器，将文件上传到webapps中，和项目同目录，files将用于保存上传的文件，将上传的文件于项目分开
        String strPath = ",webapps,files," ;

        // 解析出文件存放路径位置
        String filepath = System.getProperty("catalina.base") + strPath.replace(',', File.separatorChar);

        log.debug("文件上传路劲位置-------->>>>>>>>>>>>" + filepath);

        // 转换request，解析出request中的文件
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

        // 获取文件map集合
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();

        String fileName = null;

        // 循环遍历，取出单个文件
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {

            // 获取单个文件
            MultipartFile mf = entity.getValue();

            // 获得原始文件名
            fileName = mf.getOriginalFilename();

            // 截取文件类型; 这里可以根据文件类型进行判断
            String fileType = fileName.substring(fileName.lastIndexOf('.'));

            try {
                // 截取上传的文件名称
                String newFileName = fileName.substring(0, fileName.lastIndexOf('.'));

                log.debug("上传来的文件名称------->>>>>>>>>" + newFileName);

                // 拼接上传文件位置
                String newfilePath = filepath + File.separatorChar + newFileName + fileType;

                log.debug("拼接好的文件路径地址------------->>>>>>>>" + newfilePath);

                // 重新组装文件路径，用于保存在list集合中
                String filepathUrl = "files" + File.separatorChar  + File.separatorChar
                        + File.separatorChar + newFileName + fileType;

                log.debug("文件位置---------------->>>>>>>>>>" + filepathUrl);

                // 创建文件存放路径实例
                File dest = new File(filepath);

                // 判断文件夹不存在就创建
                if (!dest.exists()) {
                    dest.mkdirs();
                }

                // 创建文件实例
                File uploadFile = new File(newfilePath);

                // 判断文件已经存在，则删除该文件
                if (uploadFile.exists()) {
                    uploadFile.delete();
                }

                log.debug("start upload file-------------->>>>>>> " + fileName);

                // 利于spring中的FileCopyUtils.copy()将文件复制
                FileCopyUtils.copy(mf.getBytes(), uploadFile);

                // 将文件路径存入list集合中
                filePathList.add(filepathUrl);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

                log.error("upload failed. filename: " + fileName+"---->>>error message ----->>>>> "+ e.getMessage());

                return null;
            }
        }

        return filePathList;


    }

    @RequestMapping(value="/updateNewsInfo")
    public String updateNewsInfo( News news) {
        newsService.updateByPrimaryKeySelective(news);
        return "redirect:/news-tables";
    }
    @RequestMapping(value="/addNewsInfo")
    public String addNewsInfo( News news) {
        news.setNewsTime(new Date());
        news.setId(1);

        newsService.insertSelective(news);
        return "redirect:/news-tables";
    }


    @RequestMapping(value="/delectMsg")
    public String delectMsg( Integer postId) {

        openMsgService.deleteByPrimaryKey(postId);
        openPicService.delectByPostId(postId);
        return "redirect:/openmsg-tables";
    }
    @RequestMapping(value="/delectVersion")
    public String delectVersion( Integer versionId) {
        versionService.deleteByPrimaryKey(versionId);
        return "redirect:/version-tables";
    }
    @RequestMapping(value="/delectPic")
    public String delectPic( Integer opicId) {
        openPicService.deleteByPrimaryKey(opicId);
        return "redirect:/openmsg-tables";
    }

    @RequestMapping(value="/addVersion")
    public String newsinfo(Version Version) {
        Version.setVersionTime(new Date());
        versionService.insertSelective(Version);
        return "redirect:/version-tables";
    }
    @RequestMapping(value="/delectComment")
    public String delectComment( Integer commentId) {

        commentService.deleteByPrimaryKey(commentId);
        return "redirect:/comment-tables";
    }
    @RequestMapping(value="/selectMsgInfo")
    public String selectMsgInfo( Integer postId,Model model) {

        OpenMsgPage openMsg = openMsgService.selectPostId(postId);
        List<OpenPic> list = openPicService.selectByPostId(postId);
        model.addAttribute("openMsg",openMsg);
        model.addAttribute("list",list);

        return "openmsg-info";
    }
    @RequestMapping("/upload")
    @ResponseBody
    public UploadPic upload(HttpServletRequest req, List<MultipartFile> file,News news) throws IOException {
        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) req;
        String endpoint = "http://oss-cn-beijing.aliyuncs.com";
        String accessKeyId = "LTAI4FrDMPV58YFn1stankB2";
        String accessKeySecret = "G8wUIEYz0ijXe2Cbu3zcHuSqc9YjFq";
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        List<String> pic = new ArrayList<>();
        Map<String,MultipartFile> list = multiRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> a:list.entrySet()){
            String fileName = a.getKey();
            String newName = getNewFileName(a.getValue());
            PutObjectRequest putObjectRequest = new PutObjectRequest("chanjiaoyun", newName, new ByteArrayInputStream(a.getValue().getBytes()));
            ossClient.putObject(putObjectRequest);
            String real = "https://chanjiaoyun.oss-cn-beijing.aliyuncs.com/"+newName;
            pic.add(real);
        }
        ossClient.shutdown();
        UploadPic uploadPic = new UploadPic();
        uploadPic.setErrno(0);
        uploadPic.setData(pic);
        return uploadPic;
    }
    private String getNewFileName(MultipartFile file) {
        String origName = file.getOriginalFilename();
        String extName = origName.substring(origName.lastIndexOf("."));
        String newName = UUID.randomUUID().toString().replaceAll("-", "") + extName;
        return newName;
    }
}
