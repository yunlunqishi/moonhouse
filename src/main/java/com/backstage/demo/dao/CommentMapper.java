package com.backstage.demo.dao;

import com.backstage.demo.model.Comment;
import com.backstage.demo.model.vo.CommentPage;

import java.util.List;

public interface CommentMapper {
    int deleteByPrimaryKey(Integer commentId);

    int insert(Comment record);

    int insertSelective(Comment record);

    Comment selectByPrimaryKey(Integer commentId);

    int updateByPrimaryKeySelective(Comment record);

    int updateByPrimaryKey(Comment record);

    List<CommentPage> selectList();
}