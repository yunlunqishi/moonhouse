package com.backstage.demo.dao;

import com.backstage.demo.model.OpenPic;

import java.util.List;

public interface OpenPicMapper {
    int deleteByPrimaryKey(Integer opicId);

    int insert(OpenPic record);

    int insertSelective(OpenPic record);

    OpenPic selectByPrimaryKey(Integer opicId);

    int updateByPrimaryKeySelective(OpenPic record);

    int updateByPrimaryKey(OpenPic record);

    int delectByPostId(Integer postId);

    List<OpenPic> selectByPostId(Integer postId);
}