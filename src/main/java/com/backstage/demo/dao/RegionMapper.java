package com.backstage.demo.dao;

import com.backstage.demo.model.Region;

public interface RegionMapper {
    int insert(Region record);

    int insertSelective(Region record);

    String selectByCodeReturnString(String code);
}