package com.backstage.demo.dao;

import com.backstage.demo.model.Version;

import java.util.List;

public interface VersionMapper {
    int deleteByPrimaryKey(Integer versionId);

    int insert(Version record);

    int insertSelective(Version record);

    Version selectByPrimaryKey(Integer versionId);

    int updateByPrimaryKeySelective(Version record);

    int updateByPrimaryKey(Version record);

    List<Version> selectAll();
}