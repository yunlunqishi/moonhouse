package com.backstage.demo.dao;

import com.backstage.demo.model.WxUser;

import java.util.List;

public interface WxUserMapper {
    int deleteByPrimaryKey(Integer wxId);

    int insert(WxUser record);

    int insertSelective(WxUser record);

    WxUser selectByPrimaryKey(Integer wxId);

    int updateByPrimaryKeySelective(WxUser record);

    int updateByPrimaryKey(WxUser record);

    List<WxUser> selectList();
}