package com.backstage.demo.model;

public class GetInfo {
    private Integer userId;
    private Integer wxOrderId;
    private Integer aliOrderId;
    private Integer xulieId;
    private String type;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    private Integer shopId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getWxOrderId() {
        return wxOrderId;
    }

    public void setWxOrderId(Integer wxOrderId) {
        this.wxOrderId = wxOrderId;
    }

    public Integer getAliOrderId() {
        return aliOrderId;
    }

    public void setAliOrderId(Integer aliOrderId) {
        this.aliOrderId = aliOrderId;
    }

    public Integer getXulieId() {
        return xulieId;
    }

    public void setXulieId(Integer xulieId) {
        this.xulieId = xulieId;
    }
}
