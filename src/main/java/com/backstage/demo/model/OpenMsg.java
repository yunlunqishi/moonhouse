package com.backstage.demo.model;

import java.util.Date;

public class OpenMsg {
    private Integer postId;

    private String openContent;

    private Date openTime;

    private Integer wxId;

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getOpenContent() {
        return openContent;
    }

    public void setOpenContent(String openContent) {
        this.openContent = openContent;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Integer getWxId() {
        return wxId;
    }

    public void setWxId(Integer wxId) {
        this.wxId = wxId;
    }
}