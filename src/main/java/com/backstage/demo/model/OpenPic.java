package com.backstage.demo.model;

public class OpenPic {
    private Integer opicId;

    private String opicImg;

    private Integer postId;

    public Integer getOpicId() {
        return opicId;
    }

    public void setOpicId(Integer opicId) {
        this.opicId = opicId;
    }

    public String getOpicImg() {
        return opicImg;
    }

    public void setOpicImg(String opicImg) {
        this.opicImg = opicImg;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }
}