package com.backstage.demo.model;

import lombok.Data;

@Data
public class Permissions {
    private Integer id;

    private String permissionsName;

    public Permissions(Integer id, String permissionsname) {
        this.id = id;
        this.permissionsName = permissionsname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPermissionsName() {
        return permissionsName;
    }

    public void setPermissionsName(String permissionsName) {
        this.permissionsName = permissionsName;
    }
}