package com.backstage.demo.model;

public class Region {
    private Integer regionCode;

    private String regionName;

    private Integer regionLevel;

    private Integer parentRegionCode;

    private Integer district;

    private Integer city;

    private Integer province;

    public Integer getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(Integer regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Integer getRegionLevel() {
        return regionLevel;
    }

    public void setRegionLevel(Integer regionLevel) {
        this.regionLevel = regionLevel;
    }

    public Integer getParentRegionCode() {
        return parentRegionCode;
    }

    public void setParentRegionCode(Integer parentRegionCode) {
        this.parentRegionCode = parentRegionCode;
    }

    public Integer getDistrict() {
        return district;
    }

    public void setDistrict(Integer district) {
        this.district = district;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }
}