package com.backstage.demo.model;

import lombok.Data;
import javax.persistence.Transient;

import java.util.Set;

@Data
public class Role {

    private Integer id;

    private String roleName;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

 }