package com.backstage.demo.model;

import javax.persistence.Transient;
import java.util.Set;

public class RoleVO {
    private Role role;
    /**
     * 角色对应权限集合
     */
    @Transient
    private Set<Permissions> permissions;

    public RoleVO(Integer s, String admin, Set<Permissions> permissionsSet) {
        this.role.setId(s);
        this.role.setRoleName(admin);
        this.permissions = permissionsSet;
    }

    public RoleVO(Role role, Set<Permissions> permissionsSet) {
        this.role = role;
        this.permissions = permissionsSet;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<Permissions> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permissions> permissions) {
        this.permissions = permissions;
    }
}
