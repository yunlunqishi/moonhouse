package com.backstage.demo.model;

import cn.afterturn.easypoi.excel.annotation.Excel;




public class UserRun {


    private Integer userId;
    @Excel(name = "姓名",orderNum = "0")
    private String userName;
    @Excel(name = "性别",orderNum = "1")
    private String sex;
    @Excel(name = "出生日期",orderNum = "2")
    private String birth;
    @Excel(name = "联系电话",orderNum = "3")
    private String phone;
    @Excel(name = "是否购买",orderNum = "4")
    private String isPay;
    private String PayFrom;
    @Excel(name = "时间",orderNum = "5")
    private String time;
    @Excel(name = "地址",orderNum = "6")
    private String place;
    private Integer money;

    public java.lang.String convertgetPayFrom()
    {
        return PayFrom == null ? "" : PayFrom;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getIsPay() {
        return isPay;
    }

    public void setIsPay(String isPay) {
        this.isPay = isPay;
    }

    public String getPayFrom() {
        return PayFrom == null ? "" : PayFrom;
    }

    public void setPayFrom(String payFrom) {
        PayFrom = payFrom;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
