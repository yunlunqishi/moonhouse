package com.backstage.demo.model;

import org.springframework.data.annotation.Transient;

import java.util.Set;

public class UserVO {
    private User user;
    /**
     * 用户对应的角色集合
     */

    private Set<RoleVO> roles;

    public UserVO(Integer i, String wsl, String s, Set<RoleVO> roleSet) {
        this.user.setId(i);
        this.user.setUserName(wsl);
        this.user.setPassword(s);
        this.roles = roleSet;
    }
    public UserVO(User user, Set<RoleVO> roleSet) {
        this.user = user;
        this.roles = roleSet;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<RoleVO> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleVO> roles) {
        this.roles = roles;
    }
}
