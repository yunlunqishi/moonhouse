package com.backstage.demo.model;

import java.util.Date;

public class Version {
    private Integer versionId;

    /**
    * 版本号
    */
    private String versionNum;

    /**
    * 版本更新时间
    */
    private Date versionTime;

    /**
    * 版本更新内容
    */
    private String versionText;

    /**
    * 下载地址
    */
    private String downUrl;

    public Integer getVersionId() {
        return versionId;
    }

    public void setVersionId(Integer versionId) {
        this.versionId = versionId;
    }

    public String getVersionNum() {
        return versionNum;
    }

    public void setVersionNum(String versionNum) {
        this.versionNum = versionNum;
    }

    public Date getVersionTime() {
        return versionTime;
    }

    public void setVersionTime(Date versionTime) {
        this.versionTime = versionTime;
    }

    public String getVersionText() {
        return versionText;
    }

    public void setVersionText(String versionText) {
        this.versionText = versionText;
    }

    public String getDownUrl() {
        return downUrl;
    }

    public void setDownUrl(String downUrl) {
        this.downUrl = downUrl;
    }
}