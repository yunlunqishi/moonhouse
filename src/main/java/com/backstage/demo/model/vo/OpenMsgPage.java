package com.backstage.demo.model.vo;

import java.util.Date;

public class OpenMsgPage {
    private Integer postId;

    private String openContent;

    private Date openTime;

    private Integer wxId;
    private String nikeName;

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getOpenContent() {
        return openContent;
    }

    public void setOpenContent(String openContent) {
        this.openContent = openContent;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Integer getWxId() {
        return wxId;
    }

    public void setWxId(Integer wxId) {
        this.wxId = wxId;
    }

    public String getNikeName() {
        return nikeName;
    }

    public void setNikeName(String nikeName) {
        this.nikeName = nikeName;
    }
}
