package com.backstage.demo.model.vo;

public class ShopInfoPage {
    private Integer codeId;
    private Integer shopId;
    private String shopName;
    private Integer shopMarialId;
    private String MarialIdName;
    private Integer type;
    private String code;

    public Integer getCodeId() {
        return codeId;
    }

    public void setCodeId(Integer codeId) {
        this.codeId = codeId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getShopMarialId() {
        return shopMarialId;
    }

    public void setShopMarialId(Integer shopMarialId) {
        this.shopMarialId = shopMarialId;
    }

    public String getMarialIdName() {
        return MarialIdName;
    }

    public void setMarialIdName(String marialIdName) {
        MarialIdName = marialIdName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
