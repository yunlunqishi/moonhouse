package com.backstage.demo.service;

import com.backstage.demo.model.News;

import java.util.List;

public interface NewsService{


    int deleteByPrimaryKey(Integer newsId);

    int insert(News record);

    int insertSelective(News record);

    News selectByPrimaryKey(Integer newsId);

    int updateByPrimaryKeySelective(News record);

    int updateByPrimaryKey(News record);

    List<News> selectList();
}
