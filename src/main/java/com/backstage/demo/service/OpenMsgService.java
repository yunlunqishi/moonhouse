package com.backstage.demo.service;

import com.backstage.demo.model.OpenMsg;
import com.backstage.demo.model.vo.OpenMsgPage;
import com.backstage.demo.model.vo.PageData;

import java.util.List;

public interface OpenMsgService{


    int deleteByPrimaryKey(Integer postId);

    int insert(OpenMsg record);

    int insertSelective(OpenMsg record);

    OpenMsg selectByPrimaryKey(Integer postId);

    int updateByPrimaryKeySelective(OpenMsg record);

    int updateByPrimaryKey(OpenMsg record);

    List<OpenMsgPage> selectList();

    OpenMsgPage selectPostId(Integer postId);
}
