package com.backstage.demo.service;

import com.backstage.demo.model.OpenPic;

import java.util.List;

public interface OpenPicService{


    int deleteByPrimaryKey(Integer opicId);

    int insert(OpenPic record);

    int insertSelective(OpenPic record);

    OpenPic selectByPrimaryKey(Integer opicId);

    int updateByPrimaryKeySelective(OpenPic record);

    int updateByPrimaryKey(OpenPic record);

    int delectByPostId(Integer postId);

    List<OpenPic> selectByPostId(Integer postId);
}
