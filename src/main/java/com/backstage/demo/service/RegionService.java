package com.backstage.demo.service;

import com.backstage.demo.model.Region;
public interface RegionService{


    int insert(Region record);

    int insertSelective(Region record);

    String selectByCodeReturnString(String code);
}
