package com.backstage.demo.service;

import com.backstage.demo.model.User;

import java.util.List;

public interface UserService {


    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User getUserByUserName(String name);

    List<User> selectAll();
}




