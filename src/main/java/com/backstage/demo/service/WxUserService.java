package com.backstage.demo.service;

import com.backstage.demo.model.WxUser;

import java.util.List;

public interface WxUserService{


    int deleteByPrimaryKey(Integer wxId);

    int insert(WxUser record);

    int insertSelective(WxUser record);

    WxUser selectByPrimaryKey(Integer wxId);

    int updateByPrimaryKeySelective(WxUser record);

    int updateByPrimaryKey(WxUser record);

    List<WxUser> selectList();
}
