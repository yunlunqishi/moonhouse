package com.backstage.demo.service.impl;

import com.backstage.demo.model.vo.CommentPage;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.backstage.demo.dao.CommentMapper;
import com.backstage.demo.model.Comment;
import com.backstage.demo.service.CommentService;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService{

    @Resource
    private CommentMapper commentMapper;

    @Override
    public int deleteByPrimaryKey(Integer commentId) {
        return commentMapper.deleteByPrimaryKey(commentId);
    }

    @Override
    public int insert(Comment record) {
        return commentMapper.insert(record);
    }

    @Override
    public int insertSelective(Comment record) {
        return commentMapper.insertSelective(record);
    }

    @Override
    public Comment selectByPrimaryKey(Integer commentId) {
        return commentMapper.selectByPrimaryKey(commentId);
    }

    @Override
    public int updateByPrimaryKeySelective(Comment record) {
        return commentMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Comment record) {
        return commentMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<CommentPage> selectList() {
        return commentMapper.selectList();
    }

}
