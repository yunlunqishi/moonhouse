package com.backstage.demo.service.impl;
import com.backstage.demo.model.*;
import com.backstage.demo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LoginServiceImpl implements LoginService {


    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionsService permissionsService;
    @Autowired
    private UserRoleService userRoleService;
    @Autowired
    private  RolePermissionService rolePermissionService;

    @Override
    public UserVO getUserByName(String getMapByName) {
        //模拟数据库查询，正常情况此处是从数据库或者缓存查询。
        User user = userService.getUserByUserName(getMapByName);

        UserRole userRole = userRoleService.selectByUserId(user.getId());

        Role role = roleService.selectByPrimaryKey(userRole.getRoleId());

        List<RolePermission> rolePermissions = rolePermissionService.selectByRoleId(role.getId());

        Set<Permissions> permissionsSet = new HashSet<>();
        for (RolePermission rp : rolePermissions){
            Permissions permissions = permissionsService.selectByPrimaryKey(rp.getId());
            permissionsSet.add(permissions);
        }
        RoleVO roleVO = new RoleVO(role,permissionsSet);
        Set<RoleVO> roleSet = new HashSet<>();
        roleSet.add(roleVO);
        UserVO userVO = new UserVO(user,roleSet);
        return userVO;
    }


}