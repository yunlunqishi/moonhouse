package com.backstage.demo.service.impl;

import com.backstage.demo.model.vo.OpenMsgPage;
import com.backstage.demo.model.vo.PageData;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.backstage.demo.dao.OpenMsgMapper;
import com.backstage.demo.model.OpenMsg;
import com.backstage.demo.service.OpenMsgService;

import java.util.List;

@Service
public class OpenMsgServiceImpl implements OpenMsgService{

    @Resource
    private OpenMsgMapper openMsgMapper;

    @Override
    public int deleteByPrimaryKey(Integer postId) {
        return openMsgMapper.deleteByPrimaryKey(postId);
    }

    @Override
    public int insert(OpenMsg record) {
        return openMsgMapper.insert(record);
    }

    @Override
    public int insertSelective(OpenMsg record) {
        return openMsgMapper.insertSelective(record);
    }

    @Override
    public OpenMsg selectByPrimaryKey(Integer postId) {
        return openMsgMapper.selectByPrimaryKey(postId);
    }

    @Override
    public int updateByPrimaryKeySelective(OpenMsg record) {
        return openMsgMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(OpenMsg record) {
        return openMsgMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<OpenMsgPage> selectList() {
        return openMsgMapper.selectList();
    }

    @Override
    public OpenMsgPage selectPostId(Integer postId) {
        return openMsgMapper.selectPostId(postId);
    }

}
