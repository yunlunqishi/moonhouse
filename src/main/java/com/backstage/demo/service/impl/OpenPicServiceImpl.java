package com.backstage.demo.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.backstage.demo.model.OpenPic;
import com.backstage.demo.dao.OpenPicMapper;
import com.backstage.demo.service.OpenPicService;

import java.util.List;

@Service
public class OpenPicServiceImpl implements OpenPicService{

    @Resource
    private OpenPicMapper openPicMapper;

    @Override
    public int deleteByPrimaryKey(Integer opicId) {
        return openPicMapper.deleteByPrimaryKey(opicId);
    }

    @Override
    public int insert(OpenPic record) {
        return openPicMapper.insert(record);
    }

    @Override
    public int insertSelective(OpenPic record) {
        return openPicMapper.insertSelective(record);
    }

    @Override
    public OpenPic selectByPrimaryKey(Integer opicId) {
        return openPicMapper.selectByPrimaryKey(opicId);
    }

    @Override
    public int updateByPrimaryKeySelective(OpenPic record) {
        return openPicMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(OpenPic record) {
        return openPicMapper.updateByPrimaryKey(record);
    }

    @Override
    public int delectByPostId(Integer postId) {
        return openPicMapper.delectByPostId(postId);
    }

    @Override
    public List<OpenPic> selectByPostId(Integer postId) {
        return openPicMapper.selectByPostId(postId);
    }

}
