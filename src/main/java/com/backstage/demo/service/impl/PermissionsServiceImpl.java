package com.backstage.demo.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.backstage.demo.model.Permissions;
import com.backstage.demo.dao.PermissionsMapper;
import com.backstage.demo.service.PermissionsService;

@Service
public class PermissionsServiceImpl implements PermissionsService {

    @Resource
    private PermissionsMapper permissionsMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return permissionsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Permissions record) {
        return permissionsMapper.insert(record);
    }

    @Override
    public int insertSelective(Permissions record) {
        return permissionsMapper.insertSelective(record);
    }

    @Override
    public Permissions selectByPrimaryKey(Integer id) {
        return permissionsMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Permissions record) {
        return permissionsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Permissions record) {
        return permissionsMapper.updateByPrimaryKey(record);
    }

}

