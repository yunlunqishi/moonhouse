package com.backstage.demo.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.backstage.demo.dao.RegionMapper;
import com.backstage.demo.model.Region;
import com.backstage.demo.service.RegionService;
@Service
public class RegionServiceImpl implements RegionService{

    @Resource
    private RegionMapper regionMapper;

    @Override
    public int insert(Region record) {
        return regionMapper.insert(record);
    }

    @Override
    public int insertSelective(Region record) {
        return regionMapper.insertSelective(record);
    }

    @Override
    public String selectByCodeReturnString(String code) {
        return regionMapper.selectByCodeReturnString(code);
    }

}
