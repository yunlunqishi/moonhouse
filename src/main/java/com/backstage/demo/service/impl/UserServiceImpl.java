package com.backstage.demo.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.backstage.demo.model.User;
import com.backstage.demo.dao.UserMapper;
import com.backstage.demo.service.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return userMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(User record) {
        return userMapper.insert(record);
    }

    @Override
    public int insertSelective(User record) {
        return userMapper.insertSelective(record);
    }

    @Override
    public User selectByPrimaryKey(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(User record) {
        return userMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(User record) {
        return userMapper.updateByPrimaryKey(record);
    }

    @Override
    public User getUserByUserName(String name) {
        return userMapper.getUserByUserName(name);
    }

    @Override
    public List<User> selectAll() {
        return userMapper.selectAll();
    }

}




