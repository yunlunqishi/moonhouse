package com.backstage.demo.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.backstage.demo.dao.VersionMapper;
import com.backstage.demo.model.Version;
import com.backstage.demo.service.VersionService;

import java.util.List;

@Service
public class VersionServiceImpl implements VersionService{

    @Resource
    private VersionMapper versionMapper;

    @Override
    public int deleteByPrimaryKey(Integer versionId) {
        return versionMapper.deleteByPrimaryKey(versionId);
    }

    @Override
    public int insert(Version record) {
        return versionMapper.insert(record);
    }

    @Override
    public int insertSelective(Version record) {
        return versionMapper.insertSelective(record);
    }

    @Override
    public Version selectByPrimaryKey(Integer versionId) {
        return versionMapper.selectByPrimaryKey(versionId);
    }

    @Override
    public int updateByPrimaryKeySelective(Version record) {
        return versionMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Version record) {
        return versionMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<Version> selectAll() {
        return versionMapper.selectAll();
    }

}
