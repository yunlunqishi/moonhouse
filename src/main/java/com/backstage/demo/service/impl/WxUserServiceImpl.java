package com.backstage.demo.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.backstage.demo.model.WxUser;
import com.backstage.demo.dao.WxUserMapper;
import com.backstage.demo.service.WxUserService;

import java.util.List;

@Service
public class WxUserServiceImpl implements WxUserService{

    @Resource
    private WxUserMapper wxUserMapper;

    @Override
    public int deleteByPrimaryKey(Integer wxId) {
        return wxUserMapper.deleteByPrimaryKey(wxId);
    }

    @Override
    public int insert(WxUser record) {
        return wxUserMapper.insert(record);
    }

    @Override
    public int insertSelective(WxUser record) {
        return wxUserMapper.insertSelective(record);
    }

    @Override
    public WxUser selectByPrimaryKey(Integer wxId) {
        return wxUserMapper.selectByPrimaryKey(wxId);
    }

    @Override
    public int updateByPrimaryKeySelective(WxUser record) {
        return wxUserMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(WxUser record) {
        return wxUserMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<WxUser> selectList() {
        return wxUserMapper.selectList();
    }

}
